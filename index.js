'use strict';

let number1 = NaN;
	while (Number.isNaN(number1)) {
		number1 = prompt("Enter your number one", number1 || "");
		number1 = parseFloat(number1);
	}

let number2 = NaN;
	while (Number.isNaN(number2)) {
		number2 = prompt("Enter your number two", number2 || "");
		number2 = parseFloat(number2);
	}

let operation = "";
	while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/" ) {
	operation = prompt("Enter mathematical operation", operation || "");
}

console.log(calc(number1, number2, operation));

function calc (number1, number2, operation) {
	switch(operation) {
		case "+" :
		return number1 + number2;

		case "-" :
		return number1 - number2;

		case "*" :
		return number1 * number2;

		case "/" :
		return number1 / number2;
	}
}